import type { Meta, StoryObj } from '@storybook/react';
import { fn } from '@storybook/test';
import { CustomButton } from '@/app/component/Button';
import { MenuItemModelType } from '@/app/component/Button/MenuItem';

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories#default-export
const meta = {
  title: 'Example/Button',
  component: CustomButton,
  parameters: {
    // Optional parameter to center the component in the Canvas. More info: https://storybook.js.org/docs/configure/story-layout
    layout: 'centered',
  },
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ['autodocs'],

  // Use `fn` to spy on the onClick arg, which will appear in the actions panel once invoked: https://storybook.js.org/docs/essentials/actions#action-args
  args: { onClick: fn() },
} satisfies Meta<typeof CustomButton>;

export default meta;
type Story = StoryObj<typeof meta>;

const MENU_ITEM_MODEL_LIST: MenuItemModelType[] = [
  {
    text: '50ml',
    price: '$80.00',
  },
  {
    text: '30ml',
    price: '$60.00',
    description: 'Good to go',
  },
  {
    text: '5ml',
    price: '$15.00',
    tag: '3x5ml for $40.00',
  },
];

// More on writing stories with args: https://storybook.js.org/docs/writing-stories/args
export const Primary: Story = {
  args: {
    text: 'Button',
    data: MENU_ITEM_MODEL_LIST,
  },
};
