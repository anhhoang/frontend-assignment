import { CustomButton } from './component/Button';
import { MenuItemModelType } from './component/Button/MenuItem';
const MENU_ITEM_MODEL_LIST: MenuItemModelType[] = [
  {
    text: '50ml',
    price: '$80.00',
  },
  {
    text: '30ml',
    price: '$60.00',
    description: 'Good to go',
  },
  {
    text: '5ml',
    price: '$15.00',
    tag: '3x5ml for $40.00',
  },
];

export default function Home() {
  return (
    <main>
      <div>
        <CustomButton text="Buy" data={MENU_ITEM_MODEL_LIST} />
      </div>
    </main>
  );
}
