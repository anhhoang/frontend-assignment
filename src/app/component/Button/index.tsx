'use client';

import ShoppingBasketOutlinedIcon from '@mui/icons-material/ShoppingBasketOutlined';
import { Button, ButtonProps, styled } from '@mui/material';
import { useCallback, useState } from 'react';
import { grey } from '@mui/material/colors';
import { MenuItemModelType } from './MenuItem';
import { CustomMenu } from './Menu';

const ColorButton = styled(Button)<ButtonProps>(({ theme }) => ({
  color: theme.palette.getContrastText(grey[900]),
  backgroundColor: grey[900],
  '&:hover': {
    backgroundColor: grey[700],
  },
}));

type CustomButtonProps = ButtonProps & {
  text: string;
  data: MenuItemModelType[];
};

function getOffset(el: HTMLElement) {
  const rect = el.getBoundingClientRect();
  return {
    left: rect.left + window.scrollX,
    top: rect.top + window.scrollY,
  };
}

export const CustomButton = ({ text, data }: CustomButtonProps) => {
  const [open, setOpen] = useState(false);
  const [top, setTop] = useState(0);
  const [left, setLeft] = useState(0);

  const handleOpen = useCallback((event: React.MouseEvent<HTMLButtonElement>) => {
    const { top, left } = getOffset(event.currentTarget);
    setOpen((prev) => !prev);
    setTop(top);
    setLeft(left);
  }, []);

  const handleClose = useCallback((event: React.MouseEvent<HTMLButtonElement>) => {
    setOpen((prev) => !prev);
  }, []);

  return (
    <div>
      <ColorButton startIcon={<ShoppingBasketOutlinedIcon />} onClick={handleOpen} variant="contained" sx={open ? { visibility: 'hidden' } : undefined}>
        {text}
      </ColorButton>
      <CustomMenu open={open} data={data} onClick={handleClose} top={top} left={left} />
    </div>
  );
};
