import { CloseOutlined } from '@mui/icons-material';
import { Fab, Box, Popper, Grow, useMediaQuery, useTheme, Modal, Fade, Backdrop } from '@mui/material';
import { CustomMenuItem, MenuItemModelType } from './MenuItem';
import { useRef } from 'react';

type CustomMenuProps = {
  open: boolean;
  data: MenuItemModelType[];
  top: number;
  left: number;
  onClick?: React.MouseEventHandler<HTMLElement> | undefined;
};

export const CustomMenu = ({ open, data, top, left, onClick }: CustomMenuProps) => {
  const anchorRef = useRef<HTMLButtonElement>(null);
  const menuRef = useRef<HTMLButtonElement>(null);
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('sm'));

  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      open={open}
      onClose={onClick}
      closeAfterTransition
      slots={{ backdrop: Backdrop }}
      slotProps={{
        backdrop: {
          timeout: 500,
        },
      }}
      keepMounted
    >
      <Fade in={open}>
        <Box display="flex" gap={2} p={1} sx={{ position: 'absolute', flexDirection: 'row', top, left }}>
          <Fab
            ref={anchorRef}
            onClick={onClick}
            sx={{ width: 40, height: 40, backgroundColor: '#fff' }}
            aria-controls={open ? 'composition-menu' : undefined}
            aria-expanded={open ? 'true' : undefined}
            aria-haspopup="true"
          >
            <CloseOutlined />
          </Fab>
          <Popper open={open} anchorEl={anchorRef.current} role={undefined} placement="right-start" transition disablePortal>
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                style={{
                  transformOrigin: 'right start',
                  marginLeft: 16,
                }}
              >
                <Box
                  id="composition-menu"
                  ref={menuRef}
                  aria-labelledby="composition-button"
                  display="flex"
                  flexDirection="column"
                  sx={{ minWidth: matches ? 400 : 240, paddingTop: data.length  - 1}}
                >
                  {data.map((item, index) => (
                    <CustomMenuItem key={item.text} {...item} onClick={onClick} last={index == data.length - 1} position={data.length - index - 1} />
                  ))}
                </Box>
              </Grow>
            )}
          </Popper>
        </Box>
      </Fade>
    </Modal>
  );
};
