import { Box, Chip, Paper, PaperProps, Typography } from '@mui/material';
import ShoppingBasketOutlinedIcon from '@mui/icons-material/ShoppingBasketOutlined';
import { grey } from '@mui/material/colors';
import { useEffect, useRef } from 'react';

export type MenuItemModelType = {
  text: string;
  price?: string;
  description?: string;
  tag?: string;
};

type MenuItemProps = PaperProps &
  MenuItemModelType & {
    last?: boolean;
    position: number;
  };

export const CustomMenuItem = ({ text, price, description, tag, last, position, onClick }: MenuItemProps) => {
  const menuRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    if (menuRef.current && !last) {
      menuRef.current.style.transform = `translateY(-${position * 8}px)`;
    }
  }, []);

  return (
    <Paper onClick={onClick} sx={{ width: '100%', transition: 'all', transitionDuration: '1s' }} ref={menuRef}>
      <Box p={3} display="flex" flexDirection="column" flex={1}>
        <Box display="flex" flexDirection="row">
          <Box gap={1} flex={1} flexDirection="row" display="flex">
            <ShoppingBasketOutlinedIcon />
            <Typography variant="subtitle1">{text}</Typography>
          </Box>
          {price && (
            <Typography variant="subtitle1" pl={1} flex={1} textAlign="right">
              {price}
            </Typography>
          )}
        </Box>
        {description && (
          <Box mt={1}>
            <Typography variant="caption" sx={{ color: grey[500] }}>
              {description}
            </Typography>
          </Box>
        )}
        {tag && (
          <Box mt={2}>
            <Chip label={tag} sx={{ borderRadius: 1 }} />
          </Box>
        )}
      </Box>
    </Paper>
  );
};
